﻿using System;
using System.IO;
using System.Windows.Forms;
using System.Collections.Generic;
using Compression_Encryption_Lib;

namespace oop_lab6
{
    public partial class Form1 : Form
    {
        List<string> selected_files = new List<string>();

        public Form1()
        {
            InitializeComponent();
        }

        private void selectAction_Click(object sender, EventArgs e)
        {
            OpenFileDialog ofd = new OpenFileDialog();
            ofd.Multiselect = true;
            if (ofd.ShowDialog() != DialogResult.OK)
                return;

            selected_files = new List<string>(ofd.FileNames);
            filesListBox.Items.Clear();
            filesListBox.Items.AddRange(ofd.FileNames);
        }

        private void addAction_Click(object sender, EventArgs e)
        {
            OpenFileDialog ofd = new OpenFileDialog();
            ofd.Multiselect = true;
            if (ofd.ShowDialog() != DialogResult.OK)
                return;

            foreach (string f in ofd.FileNames)
                if (!selected_files.Contains(f))
                {
                    selected_files.Add(f);
                    filesListBox.Items.Add(f);
                }
        }

        private void clearAction_Click(object sender, EventArgs e)
        {
            filesListBox.Items.Clear();
            selected_files.Clear();
        }

        private void filesListBox_DragEnter(object sender, DragEventArgs e)
        {
            if (e.Data.GetDataPresent(DataFormats.FileDrop, false) == true)
            {
                e.Effect = DragDropEffects.All;
            }
        }

        private void filesListBox_DragDrop(object sender, DragEventArgs e)
        {
            string[] files = (string[])e.Data.GetData(DataFormats.FileDrop);
            foreach (string f in files)
                if (!selected_files.Contains(f))
                {
                    selected_files.Add(f);
                    filesListBox.Items.Add(f);
                }
        }

        private void encryptAction_Click(object sender, EventArgs e)
        {
            if (selected_files.Count == 0)
            {
                MessageBox.Show("Додайте файли для шифрування", "Помилка", MessageBoxButtons.OK, MessageBoxIcon.Error);
                return;
            }

            FolderBrowserDialog fbd = new FolderBrowserDialog();
            if (fbd.ShowDialog() != DialogResult.OK)
                return;

            string pathToSave = fbd.SelectedPath;
            Encryption.Encrypt(selected_files, pathToSave);
            MessageBox.Show("Файли зашифровано", "Результат", MessageBoxButtons.OK, MessageBoxIcon.Information);
        }

        private void decryptAction_Click(object sender, EventArgs e)
        {
            if (selected_files.Count == 0)
            {
                MessageBox.Show("Додайте файли для дешифрування", "Помилка", MessageBoxButtons.OK, MessageBoxIcon.Error);
                return;
            }

            FolderBrowserDialog fbd = new FolderBrowserDialog();
            if (fbd.ShowDialog() != DialogResult.OK)
                return;

            string pathToSave = fbd.SelectedPath;
            Encryption.Decrypt(selected_files, pathToSave);

            MessageBox.Show("Файли дешифровано", "Результат", MessageBoxButtons.OK, MessageBoxIcon.Information);
        }

        private void compressAction_Click(object sender, EventArgs e)
        {
            if (selected_files.Count == 0)
            {
                MessageBox.Show("Додайте файли для архівування", "Помилка", MessageBoxButtons.OK, MessageBoxIcon.Error);
                return;
            }

            SaveFileDialog sfd = new SaveFileDialog();
            sfd.Filter = "GZ Archive |*.gzar";

            if (sfd.ShowDialog() != DialogResult.OK)
                return;

            string pathToSave = sfd.FileName;
            Compression.Compress(selected_files, pathToSave);

            MessageBox.Show("Архів створено", "Результат", MessageBoxButtons.OK, MessageBoxIcon.Information);
        }

        private void decompressAction_Click(object sender, EventArgs e)
        {
            if (selected_files.Count != 1)
            {
                MessageBox.Show("Архів не вибрано або вибрано декілька", "Помилка", MessageBoxButtons.OK, MessageBoxIcon.Error);
                return;
            }

            if (!Path.GetExtension(selected_files[0]).Equals(".gzar"))
            {
                MessageBox.Show("Невідомий формат архіву. Підтримуваний формат (.gzar)", 
                    "Помилка", MessageBoxButtons.OK, MessageBoxIcon.Error);
                return;
            }

            FolderBrowserDialog fbd = new FolderBrowserDialog();
            if (fbd.ShowDialog() != DialogResult.OK)
                return;

            string pathToSave = fbd.SelectedPath;
            Compression.Decompress(selected_files[0], pathToSave);

            MessageBox.Show("Архів розпаковано", "Результат", MessageBoxButtons.OK, MessageBoxIcon.Information);
        }
    }
}
