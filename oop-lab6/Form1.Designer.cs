﻿namespace oop_lab6
{
    partial class Form1
    {
        /// <summary>
        /// Обязательная переменная конструктора.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Освободить все используемые ресурсы.
        /// </summary>
        /// <param name="disposing">истинно, если управляемый ресурс должен быть удален; иначе ложно.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Код, автоматически созданный конструктором форм Windows

        /// <summary>
        /// Требуемый метод для поддержки конструктора — не изменяйте 
        /// содержимое этого метода с помощью редактора кода.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(Form1));
            this.menuStrip1 = new System.Windows.Forms.MenuStrip();
            this.fileItem = new System.Windows.Forms.ToolStripMenuItem();
            this.selectAction = new System.Windows.Forms.ToolStripMenuItem();
            this.addAction = new System.Windows.Forms.ToolStripMenuItem();
            this.clearAction = new System.Windows.Forms.ToolStripMenuItem();
            this.encryptionMenu = new System.Windows.Forms.ToolStripMenuItem();
            this.encryptAction = new System.Windows.Forms.ToolStripMenuItem();
            this.decryptAction = new System.Windows.Forms.ToolStripMenuItem();
            this.compressMenu = new System.Windows.Forms.ToolStripMenuItem();
            this.compressAction = new System.Windows.Forms.ToolStripMenuItem();
            this.decompressAction = new System.Windows.Forms.ToolStripMenuItem();
            this.filesListBox = new System.Windows.Forms.ListBox();
            this.label1 = new System.Windows.Forms.Label();
            this.menuStrip1.SuspendLayout();
            this.SuspendLayout();
            // 
            // menuStrip1
            // 
            this.menuStrip1.ImageScalingSize = new System.Drawing.Size(20, 20);
            this.menuStrip1.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.fileItem,
            this.encryptionMenu,
            this.compressMenu});
            this.menuStrip1.Location = new System.Drawing.Point(0, 0);
            this.menuStrip1.Name = "menuStrip1";
            this.menuStrip1.Size = new System.Drawing.Size(372, 24);
            this.menuStrip1.TabIndex = 1;
            this.menuStrip1.Text = "menuStrip1";
            // 
            // fileItem
            // 
            this.fileItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.selectAction,
            this.addAction,
            this.clearAction});
            this.fileItem.Name = "fileItem";
            this.fileItem.Size = new System.Drawing.Size(48, 20);
            this.fileItem.Text = "Файл";
            // 
            // selectAction
            // 
            this.selectAction.Name = "selectAction";
            this.selectAction.Size = new System.Drawing.Size(127, 22);
            this.selectAction.Text = "Вибрати";
            this.selectAction.Click += new System.EventHandler(this.selectAction_Click);
            // 
            // addAction
            // 
            this.addAction.Name = "addAction";
            this.addAction.Size = new System.Drawing.Size(127, 22);
            this.addAction.Text = "Додати";
            this.addAction.Click += new System.EventHandler(this.addAction_Click);
            // 
            // clearAction
            // 
            this.clearAction.Name = "clearAction";
            this.clearAction.Size = new System.Drawing.Size(127, 22);
            this.clearAction.Text = "Очистити";
            this.clearAction.Click += new System.EventHandler(this.clearAction_Click);
            // 
            // encryptionMenu
            // 
            this.encryptionMenu.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.encryptAction,
            this.decryptAction});
            this.encryptionMenu.Name = "encryptionMenu";
            this.encryptionMenu.Size = new System.Drawing.Size(91, 20);
            this.encryptionMenu.Text = "Шифрування";
            // 
            // encryptAction
            // 
            this.encryptAction.Name = "encryptAction";
            this.encryptAction.Size = new System.Drawing.Size(157, 22);
            this.encryptAction.Text = "Шифрувати";
            this.encryptAction.Click += new System.EventHandler(this.encryptAction_Click);
            // 
            // decryptAction
            // 
            this.decryptAction.Name = "decryptAction";
            this.decryptAction.Size = new System.Drawing.Size(157, 22);
            this.decryptAction.Text = "Розшифрувати";
            this.decryptAction.Click += new System.EventHandler(this.decryptAction_Click);
            // 
            // compressMenu
            // 
            this.compressMenu.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.compressAction,
            this.decompressAction});
            this.compressMenu.Name = "compressMenu";
            this.compressMenu.Size = new System.Drawing.Size(86, 20);
            this.compressMenu.Text = "Архівування";
            // 
            // compressAction
            // 
            this.compressAction.Name = "compressAction";
            this.compressAction.Size = new System.Drawing.Size(150, 22);
            this.compressAction.Text = "Архівувати";
            this.compressAction.Click += new System.EventHandler(this.compressAction_Click);
            // 
            // decompressAction
            // 
            this.decompressAction.Name = "decompressAction";
            this.decompressAction.Size = new System.Drawing.Size(150, 22);
            this.decompressAction.Text = "Розархівувати";
            this.decompressAction.Click += new System.EventHandler(this.decompressAction_Click);
            // 
            // filesListBox
            // 
            this.filesListBox.AllowDrop = true;
            this.filesListBox.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.filesListBox.FormattingEnabled = true;
            this.filesListBox.Location = new System.Drawing.Point(12, 27);
            this.filesListBox.Name = "filesListBox";
            this.filesListBox.SelectionMode = System.Windows.Forms.SelectionMode.None;
            this.filesListBox.Size = new System.Drawing.Size(351, 186);
            this.filesListBox.TabIndex = 4;
            this.filesListBox.DragDrop += new System.Windows.Forms.DragEventHandler(this.filesListBox_DragDrop);
            this.filesListBox.DragEnter += new System.Windows.Forms.DragEventHandler(this.filesListBox_DragEnter);
            // 
            // label1
            // 
            this.label1.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Arial", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.label1.Location = new System.Drawing.Point(191, 234);
            this.label1.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(172, 18);
            this.label1.TabIndex = 5;
            this.label1.Text = "Ніколайчук Р.О., СН-21";
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(372, 261);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.filesListBox);
            this.Controls.Add(this.menuStrip1);
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.MainMenuStrip = this.menuStrip1;
            this.MaximumSize = new System.Drawing.Size(604, 454);
            this.MinimumSize = new System.Drawing.Size(342, 300);
            this.Name = "Form1";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Робота з файлами";
            this.menuStrip1.ResumeLayout(false);
            this.menuStrip1.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion
        private System.Windows.Forms.MenuStrip menuStrip1;
        private System.Windows.Forms.ToolStripMenuItem fileItem;
        private System.Windows.Forms.ToolStripMenuItem selectAction;
        private System.Windows.Forms.ToolStripMenuItem encryptionMenu;
        private System.Windows.Forms.ToolStripMenuItem encryptAction;
        private System.Windows.Forms.ToolStripMenuItem decryptAction;
        private System.Windows.Forms.ToolStripMenuItem compressMenu;
        private System.Windows.Forms.ToolStripMenuItem compressAction;
        private System.Windows.Forms.ToolStripMenuItem decompressAction;
        private System.Windows.Forms.ToolStripMenuItem addAction;
        private System.Windows.Forms.ToolStripMenuItem clearAction;
        private System.Windows.Forms.ListBox filesListBox;
        private System.Windows.Forms.Label label1;
    }
}

