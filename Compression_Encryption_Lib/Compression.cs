﻿using System;
using System.Text;
using System.Collections.Generic;
using System.IO;
using System.IO.Compression;

namespace Compression_Encryption_Lib
{
    public class Compression
    {
        public static void Compress(IEnumerable<string> filesToCompress, string pathToSave)
        {
            using (ZipArchive archive = ZipFile.Open(pathToSave, ZipArchiveMode.Create))
            {
                foreach (string f in filesToCompress)
                    archive.CreateEntryFromFile(f, Path.GetFileName(f));
            }
        }

        public static void Decompress(string pathToArchive, string pathToDecompress)
        {
            using (ZipArchive archive = ZipFile.Open(pathToArchive, ZipArchiveMode.Read))
            {
                archive.ExtractToDirectory(pathToDecompress);
            }
        }
    }
}
