﻿using System.Text;
using System.Collections.Generic;
using System.IO;
using System.Security.Cryptography;

namespace Compression_Encryption_Lib
{
    public class Encryption
    {
        public static byte[] Key = Encoding.ASCII.GetBytes("NdjWislKxnDjaN9S");
        public static byte[] IV = Encoding.ASCII.GetBytes("4l(f4mbld0}KW@AB");

        public static void Encrypt(IEnumerable<string> filesToEncrypt, string pathToSave)
        {
            using (AesManaged aesAlg = new AesManaged())
            {
                ICryptoTransform encryptor = aesAlg.CreateEncryptor(Key, IV);
                foreach (string f in filesToEncrypt)
                {
                    string path = $"{pathToSave}\\{Path.GetFileName(f)}.crypt";
                    using (Stream fsw = new FileStream(path, FileMode.CreateNew),
                        cs = new CryptoStream(fsw, encryptor, CryptoStreamMode.Write),
                        fsr = new FileStream(f, FileMode.Open))
                    {
                        fsr.CopyTo(cs);
                    }
                }
            }
        }

        public static void Decrypt(IEnumerable<string> filesToDecrypt, string pathToSave)
        {
            using (AesManaged aesAlg = new AesManaged())
            {
                ICryptoTransform decryptor = aesAlg.CreateDecryptor(Key, IV);
                foreach (string f in filesToDecrypt)
                {
                    string path = $"{pathToSave}\\{Path.GetFileNameWithoutExtension(f)}";
                    using (Stream fsr = new FileStream(f, FileMode.Open), 
                        cs = new CryptoStream(fsr, decryptor, CryptoStreamMode.Read), 
                        fsw = new FileStream(path, FileMode.CreateNew))
                    {
                        cs.CopyTo(fsw);
                    }
                }
            }
        }
    }
}
